<?php

namespace App;

final class Person
{
    private $name;
    private $weight;
    private $strenght;

    public function __construct($name, $weight,$strenght)
    {
        $this->name = $name;
        $this->weight = $weight;
        $this->strenght = $strenght;
    }

    public static function translate(array $strings) :array
    {
        $return = [];
        foreach ($strings as $string)
        {
            list($name, $weight, $strenght) = explode(' ', $string);
            $person = new Person($name,$weight,$strenght);
            array_push($return,$person);
        }

        return $return;
    }
    public function getName()
    {
        return $this->name;
    }
}